import base64
import enum
import os
import time

import requests
import yaml

from bitbucket_pipes_toolkit import Pipe


schema = {
    'JENKINS_URL': {'type': 'string', 'required': True},
    'JENKINS_USER': {'type': 'string', 'required': True},
    'JENKINS_TOKEN': {'type': 'string', 'required': True},
    'JOB_NAME': {'type': 'string', 'required': True},
    'JOB_PARAMETERS': {
        "empty": False,
        "required": False,
        "type": "dict",
        'excludes': 'BUILD_FROM_FILE'
    },
    'BUILD_FROM_FILE': {'type': 'string', 'required': False, 'excludes': 'JOB_PARAMETERS'},
    'JENKINS_FILE_PATH': {'type': 'string', 'required': False},
    'CERTIFICATE': {'type': 'string', 'required': False},
    'WAIT': {'type': 'boolean', 'required': False, 'default': False},
    'WAIT_MAX_TIME': {'default': 300, 'type': 'integer'},
    'WAIT_SLEEP_TIME': {'default': 5, 'type': 'integer', 'min': 5},
    'DEBUG': {'type': 'boolean', 'required': False, 'default': False}
}


@enum.unique
class BuildStatus(enum.Enum):
    succeeded = 'SUCCESS'
    failed = 'FAILURE'
    unstable = 'UNSTABLE'
    aborted = 'ABORTED'
    pending = 'PENDING'
    in_progress = 'IN_PROGRESS'


class JenkinsSubmitPipe(Pipe):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        self.wait_max_time = self.get_variable('WAIT_MAX_TIME')
        self.wait_sleep_time = self.get_variable('WAIT_SLEEP_TIME')

        self._validate_wait_parameters()

        self.jenkins_url = str(self.get_variable('JENKINS_URL')).strip('/')
        self.job_name = self.get_variable('JOB_NAME')
        self.user = self.get_variable('JENKINS_USER')
        self.token = self.get_variable('JENKINS_TOKEN')
        self.build_file = self.get_variable('BUILD_FROM_FILE')
        jenkins_file_path = self.get_variable('JENKINS_FILE_PATH')
        self.jenkins_file_path = jenkins_file_path if jenkins_file_path else self.build_file
        self.certificate = self.get_variable('CERTIFICATE')
        self.job_params = self.get_variable('JOB_PARAMETERS')

        self.verify = True

    # TODO: Refactor to use toolkit when implemented
    def _validate_wait_parameters(self):
        if self.wait_sleep_time >= self.wait_max_time:
            self.fail(message='Validation errors: Value WAIT_SLEEP_TIME should be less than WAIT_MAX_TIME.')

    def wait_for_job(self, queue_url):
        timeout = time.time() + self.wait_max_time
        build_number = None

        self.log_info(f"queue_url {queue_url}")
        while timeout > time.time():
            queue_build_response = requests.get(f'{queue_url}/api/json', auth=(self.user, self.token))
            if not queue_build_response.ok:
                self.fail(f'Queue request has failed with error: {queue_build_response.text}')
            queue_build = queue_build_response.json()
            if 'executable' in queue_build:
                try:
                    build_number = queue_build['executable']['number']
                    build_job_url = queue_build['executable']['url']
                    break
                except Exception:
                    self.log_info(f"Unknown response from Jenkins: {queue_build_response.text}")
            elif 'nextBuildNumber' in queue_build:
                build_number = queue_build['nextBuildNumber']
                build_job_url = f"{queue_build['url']}/{build_number}"
                break
            elif 'why' in queue_build:
                self.log_info(f"Jenkins build not started yet: {queue_build['why']}")

            time.sleep(self.wait_sleep_time)

        if not build_number:
            self.fail('Timeout while waiting for the Jenkins job to start')

        while timeout > time.time():
            builds_status_response = requests.get(f'{build_job_url}/api/json', auth=(self.user, self.token))
            if builds_status_response.status_code == 404:
                self.log_info(f'Current status of jenkins build with number {build_number} '
                              f'is {BuildStatus.pending.value}. Wait for build to be started...')

                time.sleep(self.wait_sleep_time)
                continue

            build_result = builds_status_response.json()
            in_progress = build_result.get('inProgress', None)
            status = build_result['result']

            if in_progress:
                self.log_info(f'Current status of jenkins build with number {build_number} is {status}. '
                              f'Wait for build to be completed...')
                time.sleep(self.wait_sleep_time)
                continue

            if status == BuildStatus.succeeded.value:
                self.success(f'Jenkins build with number {build_number} has finished successfully.')
                return
            if status == BuildStatus.unstable.value:
                self.fail(f'Jenkins build with number {build_number} has finished as unstable.')
            if status == BuildStatus.failed.value:
                self.fail(f'Jenkins build with number {build_number} has failed.')
            if status == BuildStatus.aborted.value:
                self.fail(f'Jenkins build with number {build_number} has aborted.')

            self.log_info(f'Current status of jenkins build with number {build_number} is {status}. '
                          f'Wait for build to be completed...')

            time.sleep(self.wait_sleep_time)

        self.fail(f'Timeout while waiting for jenkins job with build number {build_number} to be completed')

    def run(self):
        super().run()

        self.log_info(self.jenkins_file_path)

        self.log_info('Submitting jenkins job...')

        params = {}

        if self.certificate:
            with open('/tmp/cert.pem', 'wb') as cert_file:
                cert_file.write(base64.b64decode(self.certificate))

            self.verify = '/tmp/cert.pem'

        if self.get_variable('DEBUG'):
            params.update({'verbosity': 'high'})

        if self.job_params:
            params.update(self.job_params)

        if self.build_file and not os.path.exists(self.build_file):
            self.fail(f'Jenkins job file {self.build_file} does not exist')

        payload = None
        headers = {}
        file = None

        if self.build_file:
            parameter = f'{{"parameter": [{{"name": "{self.jenkins_file_path}",  "file":"file0" }}]}}'
            file = open(self.build_file, "rb")
            payload = (
                ('file0', file),
                ('json', parameter)
            )

        if self.job_params:
            final_build_uri = 'buildWithParameters'
        else:
            final_build_uri = 'build'

        build_response = requests.post(
            f'{self.jenkins_url}/{self.job_name}/{final_build_uri}',
            data=params,
            files=payload, headers=headers,
            auth=(self.user, self.token),
            verify=self.verify
        )

        if file:
            file.close()

        if not build_response.ok:
            self.fail(f'Pipe has failed with error: {build_response.text}')

        if self.get_variable('WAIT'):
            self.wait_for_job(build_response.headers['Location'])

        self.success('Pipe has finished successfully.')


if __name__ == '__main__':
    with open('/pipe.yml') as f:
        metadata = yaml.safe_load(f.read())
    pipe = JenkinsSubmitPipe(pipe_metadata=metadata, schema=schema, check_for_newer_version=True)
    pipe.run()
