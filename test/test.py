import os
import time

import requests
from bitbucket_pipes_toolkit.test import PipeTestCase


def wait_for_jenkins_to_start(jenkins_container, warmup_time):

    timeout = time.time() + warmup_time
    while time.time() < timeout:
        logs = jenkins_container.logs()
        if b'Jenkins is fully up and running' in logs:
            return
        time.sleep(3)
    else:
        raise Exception('Timeout for jenkins container has exceeded')


def setup_credentials(session, password):
    crumb_jenkins_url = 'http://localhost:8080/crumbIssuer/api/json'
    token_name = 'test-bbci-token'
    token_generate_url = (f'http://localhost:8080/user/admin/descriptorByName/jenkins.security.ApiTokenProperty/'
                          f'generateNewToken?newTokenName={token_name}')

    crumb_response = session.get(crumb_jenkins_url,
                                 auth=(os.getenv('JENKINS_USER'), password))
    crumb = crumb_response.json()
    crumb_headers = {crumb['crumbRequestField']: crumb['crumb']}

    token_response = session.post(
        token_generate_url,
        auth=(os.getenv('JENKINS_USER'), password),
        headers=crumb_headers
    )
    jenkins_token = token_response.json()['data']['tokenValue']
    return jenkins_token


def create_jenkins_jobs(session, token):

    job_name_default = os.getenv('JOB_NAME_DEFAULT')
    with open('test/mylocalconfig.xml') as f:
        content = f.read()
    session.post(f'http://localhost:8080/createItem?name={job_name_default}', data=content,
                 headers={'Content-Type': 'text/xml'}, auth=(os.getenv('JENKINS_USER'), token))

    job_name_parametrized = os.getenv('JOB_NAME_PARAMETRIZED')
    with open('test/config_parametrized.xml') as f:
        content = f.read()
    session.post(f'http://localhost:8080/createItem?name={job_name_parametrized}', data=content,
                 headers={'Content-Type': 'text/xml'}, auth=(os.getenv('JENKINS_USER'), token))

    job_name_with_file = os.getenv('JOB_NAME_WITH_FILE')
    with open('test/config_with_file.xml') as f:
        content = f.read()
    session.post(f'http://localhost:8080/createItem?name={job_name_with_file}', data=content,
                 headers={'Content-Type': 'text/xml'}, auth=(os.getenv('JENKINS_USER'), token))


class JenkinsTestCase(PipeTestCase):
    jenkins_container_name = 'jenkins'
    JENKINS_WARMUP_TIME = 60  # in seconds

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        jenkins_container = cls.docker_client.containers.get(cls.jenkins_container_name)
        wait_for_jenkins_to_start(jenkins_container, warmup_time=cls.JENKINS_WARMUP_TIME)

        password = jenkins_container.exec_run(
            'cat /var/jenkins_home/secrets/initialAdminPassword').output.decode('utf-8').strip('\n')
        with requests.Session() as session:
            cls.jenkins_token = setup_credentials(session, password)
            create_jenkins_jobs(session, cls.jenkins_token)
        jenkins_internal_ip = cls.docker_client.api.inspect_container(
            os.getenv('JENKINS_CONTAINER_NAME', 'jenkins'))['NetworkSettings']['IPAddress']
        cls.jenkins_url = f'http://{jenkins_internal_ip}:8080/job'

    def test_success(self):
        result = self.run_container(environment={
            'JENKINS_URL': self.jenkins_url,
            'JENKINS_USER': os.getenv('JENKINS_USER'),
            'JENKINS_TOKEN': self.jenkins_token,
            'JOB_NAME': os.getenv('JOB_NAME_DEFAULT'),
            'WAIT': True,
        })
        self.assertRegex(result, r'✔ Jenkins build with number \d+ has finished successfully')
        self.assertIn('✔ Pipe has finished successfully.', result)

    def test_success_with_parameters(self):
        job_parameters = '''
        {
            'test': 'test value changed'
        }
        '''
        result = self.run_container(environment={
            'JENKINS_URL': self.jenkins_url,
            'JENKINS_USER': os.getenv('JENKINS_USER'),
            'JENKINS_TOKEN': self.jenkins_token,
            'JOB_NAME': os.getenv('JOB_NAME_PARAMETRIZED'),
            'JOB_PARAMETERS': job_parameters,
            'WAIT': True
        })
        self.assertRegex(result, r'✔ Jenkins build with number \d+ has finished successfully')
        self.assertIn('✔ Pipe has finished successfully.', result)

    def test_success_from_file(self):
        result = self.run_container(environment={
            'JENKINS_URL': self.jenkins_url,
            'JENKINS_USER': os.getenv('JENKINS_USER'),
            'JENKINS_TOKEN': self.jenkins_token,
            'JOB_NAME': os.getenv('JOB_NAME_WITH_FILE'),
            'BUILD_FROM_FILE': 'test/Jenkinsfile',
            'JENKINS_FILE_PATH': 'test/jenkins',
            'WAIT': True
        })
        self.assertRegex(result, r'✔ Jenkins build with number \d+ has finished successfully')
        self.assertIn('✔ Pipe has finished successfully.', result)

    def test_error_path_mismatch_from_file(self):
        result = self.run_container(environment={
            'JENKINS_URL': self.jenkins_url,
            'JENKINS_USER': os.getenv('JENKINS_USER'),
            'JENKINS_TOKEN': self.jenkins_token,
            'JOB_NAME': os.getenv('JOB_NAME_WITH_FILE'),
            'BUILD_FROM_FILE': 'test/Jenkinsfile',
            'WAIT': True
        })
        self.assertIn('✖ Pipe has failed with error', result)

    def test_fail_file_not_found(self):
        result = self.run_container(environment={
            'JENKINS_URL': self.jenkins_url,
            'JENKINS_USER': os.getenv('JENKINS_USER'),
            'JENKINS_TOKEN': self.jenkins_token,
            'JOB_NAME': os.getenv('JOB_NAME_DEFAULT'),
            'BUILD_FROM_FILE': 'wrong-file-location',
        })
        self.assertIn(r'✖ Jenkins job file wrong-file-location does not exist', result)

    def test_fail_waiting_for_job_timeout(self):
        result = self.run_container(environment={
            'JENKINS_URL': self.jenkins_url,
            'JENKINS_USER': os.getenv('JENKINS_USER'),
            'JENKINS_TOKEN': self.jenkins_token,
            'JOB_NAME': os.getenv('JOB_NAME_DEFAULT'),
            'WAIT': True,
            'WAIT_MAX_TIME': 6
        })
        self.assertIn('✖ Timeout while waiting for the Jenkins job to start', result)
