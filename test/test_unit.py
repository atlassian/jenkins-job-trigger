from contextlib import contextmanager
from copy import copy
import io
import os
import sys

from unittest import mock, TestCase


@contextmanager
def capture_output():
    standard_out = sys.stdout
    try:
        stdout = io.StringIO()
        sys.stdout = stdout
        yield stdout
    finally:
        sys.stdout = standard_out
        sys.stdout.flush()


@mock.patch('requests.post', side_effect=[mock.Mock(**{'status_code': 201, 'ok': True,
                                                       'headers': {'Location': 'queue/item/33'}})
                                          ]
            )
@mock.patch('requests.get')
class JenkinsUnitBuildTestCase(TestCase):

    def setUp(self):
        self.sys_path = copy(sys.path)
        sys.path.insert(0, os.getcwd())

    def tearDown(self):
        sys.path = self.sys_path

    @mock.patch.dict(os.environ, {'JOB_NAME': 'test', 'JENKINS_USER': 'test', 'JENKINS_TOKEN': '123fde',
                                  'WAIT': 'true', 'WAIT_MAX_TIME': '1', 'JENKINS_URL': 'http://fake:8000'})
    @mock.patch('pipe.pipe.JenkinsSubmitPipe._validate_wait_parameters')
    def test_queue_unknown_result(self, mock_validate, mock_get_build, mock_post):
        from pipe.pipe import JenkinsSubmitPipe, schema

        mock_get_build.return_value.json.side_effect = [{'executable': 'unknown'}]

        pipe = JenkinsSubmitPipe(schema=schema, check_for_newer_version=True)
        with capture_output() as out:
            with self.assertRaises(SystemExit) as exc_context:
                pipe.run()
            self.assertEqual(exc_context.exception.code, 1)
        self.assertRegex(out.getvalue(), '✖ Timeout while waiting for the Jenkins job to start.')

    @mock.patch.dict(os.environ, {'JOB_NAME': 'test', 'JENKINS_USER': 'test', 'JENKINS_TOKEN': '123fde',
                                  'WAIT': 'true', 'WAIT_MAX_TIME': '1', 'JENKINS_URL': 'http://fake:8000'})
    @mock.patch('pipe.pipe.JenkinsSubmitPipe._validate_wait_parameters')
    def test_queue_pending_timeout(self, mock_validate, mock_get_build, mock_post):
        from pipe.pipe import JenkinsSubmitPipe, schema

        mock_get_build.return_value.json.side_effect = [{'why': 'Waiting for an agent.'}]

        pipe = JenkinsSubmitPipe(schema=schema, check_for_newer_version=True)
        with capture_output() as out:
            with self.assertRaises(SystemExit) as exc_context:
                pipe.run()
            self.assertEqual(exc_context.exception.code, 1)
        self.assertRegex(out.getvalue(), '✖ Timeout while waiting for the Jenkins job to start.')

    @mock.patch.dict(os.environ, {'JOB_NAME': 'test', 'JENKINS_USER': 'test', 'JENKINS_TOKEN': '123fde',
                                  'WAIT': 'true', 'WAIT_MAX_TIME': '1', 'JENKINS_URL': 'http://fake:8000'})
    @mock.patch('pipe.pipe.JenkinsSubmitPipe._validate_wait_parameters')
    def test_status_pending_timeout(self, mock_validate, mock_get_build, mock_post):
        from pipe.pipe import JenkinsSubmitPipe, schema

        mock_get_build.return_value.json.side_effect = [{'executable': {'number': 5, 'url': 'http://fake:8000/job/test'}},
                                                        {'result': 'PENDING'}
                                                        ]

        pipe = JenkinsSubmitPipe(schema=schema, check_for_newer_version=True)
        with capture_output() as out:
            with self.assertRaises(SystemExit) as exc_context:
                pipe.run()
            self.assertEqual(exc_context.exception.code, 1)
        self.assertRegex(out.getvalue(), '✖ Timeout while waiting for jenkins job with build number 5 to be completed')

    @mock.patch.dict(os.environ, {'JOB_NAME': 'test', 'JENKINS_USER': 'test', 'JENKINS_TOKEN': '123fde',
                                  'WAIT': 'true', 'JENKINS_URL': 'http://fake:8000'})
    def test_status_aborted(self, mock_get_build, _):
        from pipe.pipe import JenkinsSubmitPipe, schema

        mock_get_build.return_value.json.side_effect = [{'executable': {'number': 5, 'url': 'http://fake:8000/job/test'}},
                                                        {'result': 'ABORTED'}
                                                        ]
        pipe = JenkinsSubmitPipe(schema=schema, check_for_newer_version=True)
        with capture_output() as out:
            with self.assertRaises(SystemExit) as exc_context:
                pipe.run()
            self.assertEqual(exc_context.exception.code, 1)
        self.assertRegex(out.getvalue(), '✖ Jenkins build with number 5 has aborted')

    @mock.patch.dict(os.environ, {'JOB_NAME': 'test', 'JENKINS_USER': 'test', 'JENKINS_TOKEN': '123fde',
                                  'WAIT': 'true', 'JENKINS_URL': 'http://fake:8000'})
    def test_status_in_progress_and_success(self, mock_get_build, _):
        from pipe.pipe import JenkinsSubmitPipe, schema

        mock_get_build.return_value.json.side_effect = [{'executable': {'number': 5, 'url': 'http://fake:8000/job/test'}},
                                                        {'result': None},
                                                        {'result': 'SUCCESS'}
                                                        ]
        pipe = JenkinsSubmitPipe(schema=schema, check_for_newer_version=True)
        with capture_output() as out:
            pipe.run()

        self.assertRegex(out.getvalue(), '✔ Jenkins build with number 5 has finished successfully.')
        self.assertRegex(out.getvalue(), '✔ Pipe has finished successfully.')

    @mock.patch.dict(os.environ, {'JOB_NAME': 'test', 'JENKINS_USER': 'test', 'JENKINS_TOKEN': '123fde',
                                  'WAIT': 'true', 'JENKINS_URL': 'http://fake:8000'})
    def test_status_failed(self, mock_get_build, _):
        from pipe.pipe import JenkinsSubmitPipe, schema

        mock_get_build.return_value.json.side_effect = [{'executable': {'number': 5, 'url': 'http://fake:8000/job/test'}},
                                                        {'result': 'FAILURE'}
                                                        ]
        pipe = JenkinsSubmitPipe(schema=schema, check_for_newer_version=True)
        with capture_output() as out:
            with self.assertRaises(SystemExit) as exc_context:
                pipe.run()
            self.assertEqual(exc_context.exception.code, 1)

        self.assertRegex(out.getvalue(), '✖ Jenkins build with number 5 has failed.')

    @mock.patch.dict(os.environ, {'JOB_NAME': 'test', 'JENKINS_USER': 'test', 'JENKINS_TOKEN': '123fde',
                                  'WAIT': 'true', 'JENKINS_URL': 'http://fake:8000'})
    def test_status_unstable(self, mock_get_build, _):
        from pipe.pipe import JenkinsSubmitPipe, schema

        mock_get_build.return_value.json.side_effect = [{'executable': {'number': 5, 'url': 'http://fake:8000/job/test'}},
                                                        {'result': 'UNSTABLE'}
                                                        ]
        pipe = JenkinsSubmitPipe(schema=schema, check_for_newer_version=True)
        with capture_output() as out:
            with self.assertRaises(SystemExit) as exc_context:
                pipe.run()
            self.assertEqual(exc_context.exception.code, 1)

        self.assertRegex(out.getvalue(), '✖ Jenkins build with number 5 has finished as unstable.')

    @mock.patch.dict(os.environ, {'JOB_NAME': 'test', 'JENKINS_USER': 'test', 'JENKINS_TOKEN': '123fde',
                                  'JENKINS_URL': 'http://fake:8000', 'CERTIFICATE': 'aGkydQ=='})
    def test_decode_success(self, mock_get_build, _):
        from pipe.pipe import JenkinsSubmitPipe, schema

        mock_get_build.return_value.json.side_effect = {'result': 'SUCCESS'}

        pipe = JenkinsSubmitPipe(schema=schema, check_for_newer_version=True)
        with capture_output() as out:
            pipe.run()

        self.assertRegex(out.getvalue(), '✔ Pipe has finished successfully.')

        with open('/tmp/cert.pem') as file:
            content = file.read()

        self.assertEqual(content, 'hi2u')


class JenkinsUnitValidateTestCase(TestCase):

    def setUp(self):
        self.sys_path = copy(sys.path)
        sys.path.insert(0, os.getcwd())

    def tearDown(self):
        sys.path = self.sys_path

    @mock.patch.dict(os.environ, {'JOB_NAME': 'test', 'JENKINS_USER': 'test', 'JENKINS_TOKEN': '123fde',
                                  'JENKINS_URL': 'http://fake:8000', 'WAIT_MAX_TIME': '300',
                                  'WAIT_SLEEP_TIME': '301'})
    def test_validate_wait_params(self):
        from pipe.pipe import JenkinsSubmitPipe, schema

        with capture_output() as out:
            with self.assertRaises(SystemExit) as exc_context:
                JenkinsSubmitPipe(schema=schema, check_for_newer_version=True).run()
                self.assertEqual(exc_context.exception.code, 1)

        self.assertRaises(SystemExit)
        self.assertRegex(out.getvalue(), '✖ Validation errors: Value WAIT_SLEEP_TIME should be less than WAIT_MAX_TIME.')
