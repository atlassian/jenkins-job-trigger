# Bitbucket Pipelines Pipe: Jenkins job trigger

Submit a build to Jenkins. For configuration details, refer to [Jenkins docs][Jenkins docs] and [Jenkins remote access api][Jenkins remote access api].


## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: atlassian/jenkins-job-trigger:0.10.0
  variables:
    JENKINS_URL: '<string>'
    JENKINS_USER: '<string>'
    JENKINS_TOKEN: '<string>'
    JOB_NAME: '<string>'
    # JOB_PARAMETERS: '<json>' # Optional
    # BUILD_FROM_FILE: '<string>' # Optional
    # JENKINS_FILE_PATH: '<string>' # Optional
    # CERTIFICATE: '<string>' # Optional
    # WAIT: '<boolean>' # Optional
    # WAIT_MAX_TIME: '<integer>' # Optional
    # WAIT_SLEEP_TIME: '<integer>' # Optional
    # DEBUG: '<boolean>' # Optional
```

## Variables

| Variable          | Usage                                                                                                                                                                                                  |
|-------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| JENKINS_URL (*)   | Jenkins url to trigger job. It should consist of http(s)://host:port/(uri). Uri is optional.                                                                                                           |
| JENKINS_USER (*)  | Jenkins user to authenticate.                                                                                                                                                                          |
| JENKINS_TOKEN (*) | Jenkins token to authenticate.                                                                                                                                                                         |
| JOB_NAME (*)      | Job name to trigger.                                                                                                                                                                                   |
| JOB_PARAMETERS    | Additional parameters to pass to job trigger. Excludes `BUILD_FROM_FILE`                                                                                                                               |
| BUILD_FROM_FILE   | Path to jenkins job build file from repo. Excludes `JOB_PARAMETERS`                                                                                                                                    |
| JENKINS_FILE_PATH | Path to build file to pass to jenkins job parameter. Default: `BUILD_FROM_FILE`. Provide this if relative path on jenkins not equal relative path from repo.                                           |
| CERTIFICATE       | Option to provide your self-signed certificate. Provide base64 encoded content of certificate file. To encode this content, follow [encode private key doc](#markdown-header-encode-string-to-base64). |
| WAIT              | Wait for triggered job to complete. Default: `false`.                                                                                                                                                  |
| WAIT_MAX_TIME     | Time to wait for triggered Jenkins job. Default: `300` sec.                                                                                                                                            |
| WAIT_SLEEP_TIME   | Time to wait between requests to Jenkins API. Default: `5` sec. Minimum: `5` sec. Maximum should be less than `WAIT_MAX_TIME`.                                                                         |
| DEBUG             | Turn on extra debug information. Default: `false`.                                                                                                                                                     |
_(*) = required variable. This variable needs to be specified always when using the pipe._


## Prerequisites

To use this pipe you need to get to know JENKINS_URL - the base of url to trigger Jenkins job
and generate token for Jenkins job trigger. For configuration details refer to [Jenkins docs].
You can also trigger job passing a file for build or use job parameters for a job.

Note: you should select just one option from passing job parameters or build from file.

### Encode string to base64

To encode you certificate for `CERTIFICATE` variable use base64:

**Linux**

`base64 -w 0 < my_ssh_key`

**Mac OS X**

`base64 < my_ssh_key`

## Examples


### Basic example:

Trigger Jenkins job build.

```yaml
script:
  - pipe: atlassian/jenkins-job-trigger:0.10.0
    variables:
      JENKINS_URL: 'http://my-jenkinsio-host:8080/job'
      JENKINS_USER: $JENKINS_USER
      JENKINS_TOKEN: $JENKINS_TOKEN
      JOB_NAME: 'staging-awesome-project-job'
```

### Advanced example:

Trigger Jenkins job build and wait for build to complete. Be aware to set sufficient for you `WAIT_MAX_TIME` time:

```yaml
script:
  - pipe: atlassian/jenkins-job-trigger:0.10.0
    variables:
      JENKINS_URL: 'http://my-jenkinsio-host/job'
      JENKINS_USER: $JENKINS_USER
      JENKINS_TOKEN: $JENKINS_TOKEN
      JOB_NAME: 'staging-awesome-project-job'
      WAIT: 'true'
      WAIT_MAX_TIME: 120
```

Trigger Jenkins job build with using a self-signed certificate:

```yaml
script:
  - pipe: atlassian/jenkins-job-trigger:0.10.0
    variables:
      JENKINS_URL: 'http://my-jenkinsio-host/job'
      JENKINS_USER: $JENKINS_USER
      JENKINS_TOKEN: $JENKINS_TOKEN
      JOB_NAME: 'staging-awesome-project-job'
      CERTIFICATE: '<base64 encoded string from repository variables>'
```

Trigger Jenkins job build with parameters and wait for build to complete. Be aware to set sufficient for you `WAIT_MAX_TIME` time:

```yaml
script:
  - pipe: atlassian/jenkins-job-trigger:0.10.0
    variables:
      JENKINS_URL: 'http://my-jenkinsio-host/job'
      JENKINS_USER: $JENKINS_USER
      JENKINS_TOKEN: $JENKINS_TOKEN
      JOB_NAME: 'staging-awesome-project-job'
      JOB_PARAMETERS: >
        {
           "test1": $TEST1,
           "test2": "test2"
        }
      WAIT: 'true'
      WAIT_MAX_TIME: 120
```

Trigger Jenkins job build from file and wait for build to complete. Set custom `WAIT_SLEEP_TIME`. Be aware to set sufficient for you `WAIT_MAX_TIME` time:

```yaml
script:
  - pipe: atlassian/jenkins-job-trigger:0.10.0
    variables:
      JENKINS_URL: 'http://my-jenkinsio-host/job'
      JENKINS_USER: $JENKINS_USER
      JENKINS_TOKEN: $JENKINS_TOKEN
      JOB_NAME: 'staging-awesome-project-job'
      BUILD_FROM_FILE: 'path_to_file_from_repo/jenkins_file'
      JENKINS_FILE_PATH: 'path_to_file_on_jenkins_job_parameter/jenkins_file'
      WAIT: 'true'
      WAIT_MAX_TIME: 120
      WAIT_SLEEP_TIME: 10
```


## Support
If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community][community].

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce


## License
Copyright (c) 2021 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.


[community]: https://community.atlassian.com/t5/forums/postpage/board-id/bitbucket-questions?add-tags=bitbucket-pipelines,pipes,jenkins
[Jenkins docs]: https://www.jenkins.io/doc/
[Jenkins remote access api]: https://www.jenkins.io/doc/book/using/remote-access-api/
[Jenkins wiki remote access api]: https://wiki.jenkins.io/display/JENKINS//Remote+access+API
