# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.10.0

- minor: fix: proper handling of inProgress build, 'unstable' build status.

## 0.9.0

- minor: fix: don't fail with unknown jenkins response.

## 0.8.1

- patch: Internal maintenance: Bump release pipe version. Add multi-platform build: linux/arm64 and linux/amd64.

## 0.8.0

- minor: Implement a feature to set custom sleep time with WAIT_SLEEP_TIME variable.
- patch: Internal maintenance: Update pipes versions and python packages.

## 0.7.0

- minor: fix: waiting wrong build number in jenkins.

## 0.6.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 0.5.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 0.4.0

- minor: Update bitbucket-pipes-toolkit to fix vulnerability with certify.

## 0.3.1

- patch: Internal maintenance: fix anchor links in README.

## 0.3.0

- minor: Add support of the self-signed certificate usage.
- patch: Internal maintenance: update release process to registry.

## 0.2.0

- minor: Add support of BUILD_FROM_FILE variable to trigger job with file parameters. Allow to pass files to jenkins job.
- minor: Add support of custom jenkins job path to file parameter
- minor: Fix issues with job parameters. Refactor passing parameters to jenkins. Refactor jenkins job url prefix from build to buildWithParameters.
- minor: Update README with examples of JOB_PARAMETERS and BUILD_FROM_FILE usage.
- patch: Refactor logic of WAIT for trigger job build.
- patch: Update dependencies in test/requirements

## 0.1.2

- patch: Fix the readme example.
- patch: Internal maintenance: Bump bitbucket-pipe-release version.
- patch: Internal maintenance: Update package versions in requirements and test requirements.

## 0.1.1

- patch: Fix pipe.yml.

## 0.1.0

- minor: Trigger Jenkins Job: initial release.
